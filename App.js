import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import MainTab from './components/MainTab';
import LoginPage from './page/LoginPage';
import MainPage from './page/MainPage';

const viewStyle = {
  flex: 1,
  backgroundColor: '#800000',
  alignItems: 'center',
  justifyContent: 'center',
}
const App = () => {
  const Stack = createNativeStackNavigator()
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginPage}
          // options={{
          //   headerShown: false
          // }}
        />
        {/* <Stack.Screen
          name="MainTab"
          component={MainTab}
          // options={{ title: 'Welcome' }}
        /> */}
        {/* <Stack.Screen
          name="History"
          component={HistoryPage}
          options={{ title: 'Welcome History' }}
        /> */}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
