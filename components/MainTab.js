import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import HistoryPage from '../page/HistoryPage'
import JobsPage from '../page/JobsPage'
import MainPage from '../page/MainPage'
import Icon from 'react-native-vector-icons/FontAwesome';
const MainTab = () => {
    const Tab = createBottomTabNavigator()
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={MainPage} options={{
                tabBarIcon: ({ color, size }) => (
                    <Icon name="home" size={30} color="#900" />
                )
            }} />
            <Tab.Screen name="Jobs" component={JobsPage} options={{
                tabBarIcon: ({ color, size }) => (
                    <Icon name="list" size={30} color="#900" />
                )
            }} />
            <Tab.Screen name="History" component={HistoryPage} options={{
                tabBarIcon: ({ color, size }) => (
                    <Icon name="history" size={30} color="#900" />
                )
            }} />
        </Tab.Navigator>
    )
}

export default MainTab

const styles = StyleSheet.create({})
