import React, { useState } from 'react'
import { Button, StyleSheet, Text, TextInput, View } from 'react-native'
import MainTab from '../components/MainTab'

const LoginPage = ({ navigation, route }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [token, setToken] = useState()
    const onLogin = (username, password) => {
        // navigation.replace('MainTab', { name: 'Jane' })
        navigation.setOptions({headerShown: false})
        setToken(true)
    }
    const loginComponent = (
        <View style={styles}>
            <Text>Hello, I am your cat!</Text>
            <TextInput
                placeholder='Username'
                value={username}
                onChangeText={setUsername}
            />
            <TextInput
                placeholder='Password'
                value={password}
                onChangeText={setPassword}
                secureTextEntry
            />
            <Button title="Login" color="#841584" onPress={() => onLogin(username, password)} />
        </View>
    )
    const render = !token ? loginComponent : <MainTab/>
    return render
    
}

export default LoginPage

const styles = StyleSheet.create({})
