import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const JobsPage = ({navigation, route}) => {
    return (
        <View style={styles.background}>
            <Text>Jobs</Text>
        </View>
    )
}

export default JobsPage

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#900'
    }
})
