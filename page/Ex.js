import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Ex = ({navigation, route}) => {
    return (
        <View style={styles.background}>
            <Text>Ex Page</Text>
        </View>
    )
}

export default Ex

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#900'
    }
})
